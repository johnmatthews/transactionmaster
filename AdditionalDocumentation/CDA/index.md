<sup>This documentation was created the_current_time</sup>

![Logo](./img/lixi_logo_100.png)

### LIXI CDA-this_version_number_dots Overview

This documentation describes the LIXI Credit Decision Standard for Australia (CDA this_version_number_dots) that was derived from the [LIXI Master Schema vmaster_version_number_dots](../../master/master_version_number_underscores/index.html) and supersedes all earlier versions. The announcement page for this release can be found on the LIXI web site: [LIXI Credit Decision for Australia (CDA this_version_number_dots) Standard](https://lixi.org.au/lixi-standards/lixi-cda-this_version_number_underscores-standard/).


### Purpose & Intended Audience

This documentation provides the data dictionary, glossary and detailed specifications that describe the LIXI Credit Decision Standard for Australia (CDA this_version_number_dots). The intended audience members are those Licensees or Members that are contributing to the standard, have an interest in, or are planning to implement the LIXI Credit Decision Standard for Australia.


### Release Contents

All the components of this release are available to Licensees and Members in the secure area of the LIXI Website [here](https://lixi.org.au/lixi-standards/lixi-cda-this_version_number_underscores-standard/).

### Root Element

The Root Element of a LIXI conformant CDA Message is the '[Package](../this_version_number_underscores/element/index.html#Package)' Element.

### Change Management

The source code for the LIXI Standards are now hosted in [LIXILab](https://standards.lixi.org.au/) (LIXI's GitLab Repository). Please contact LIXI if you would like access. To get further information on the Change Management Processes for this standard, or to request a change, please visit the Change Management page on the LIXI website [here](https://lixi.org.au/dc-cal-2-0-change-request/).

### Statement of Proprietary Information

This standard is confidential to LIXI Limited and may not be disclosed, duplicated, or used for any purpose, in whole or in part, without the prior consent of LIXI Limited. Both the XML Schema Definition files and associated documentation are licensed under the LIXI End User Licence Agreement (EULA) that can be found here: [LIXI-End-User-Licence-Agreement](https://lixi.org.au/assets/LIXI-End-User-Licence-Agreement-2017-11.pdf). An introduction to this EULA is available here: [Introduction to EULA 2013 10 V.2.pdf](https://cdn.lixi.org.au/assets/Introduction-to-EULA-2013-10-V.2.pdf).