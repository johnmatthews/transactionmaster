![Logo](./img/lixi_logo_100.png)


### New In LIXI CDA.2.0.5

This section contains a list of the new items that have been added to LIXI CDA.2.0.4 to create LIXI CDA.2.0.5.<br><br>
<a href="../element/index.html#Package.Content.Application.Household.Dependant.FinancialProvider" class="tooltip-parent">Package.Content.Application.Household.Dependant.FinancialProvider<span class="tooltiptext-under">A party that provides financial support to the dependant.</span> <span class="tooltiptext-after"/></a> 


<a href="../attribute/index.html#Package.Content.Application.Household.Dependant.FinancialProvider.x_Party" class="tooltip-parent">Package.Content.Application.Household.Dependant.FinancialProvider.x_Party<span class="tooltiptext-under">A cross reference to the party that provides financial support to the dependant.</span> <span class="tooltiptext-after"/></a> 


<a href="../attribute/index.html#Package.Content.Application.Household.ExpenseDetails.OtherCommitment.IsContinuing" class="tooltip-parent">Package.Content.Application.Household.ExpenseDetails.OtherCommitment.IsContinuing<span class="tooltiptext-under">Indicates that this commitment is continuing.</span> <span class="tooltiptext-after"/></a> 


<a href="../attribute/index.html#Package.Content.Application.Liability.ClearingFromOtherSourceAmount" class="tooltip-parent">Package.Content.Application.Liability.ClearingFromOtherSourceAmount<span class="tooltiptext-under">Captures the exact amount that will be refinanced for this liability using funds from a source that is not the new loan.</span> <span class="tooltiptext-after"/></a> 


<a href="../attribute/index.html#Package.Content.Application.Liability.ClearingFromThisLoanAmount" class="tooltip-parent">Package.Content.Application.Liability.ClearingFromThisLoanAmount<span class="tooltiptext-under">Captures the exact amount that will be refinanced for this liability using funds sourced from a new loan.</span> <span class="tooltiptext-after"/></a> 


<a href="../element/index.html#Package.Content.Application.Liability.ContinuingRepayment" class="tooltip-parent">Package.Content.Application.Liability.ContinuingRepayment<span class="tooltiptext-under">Details of the continuing repayment expense associated with this liability where it differs from the current repayment expense.</span> <span class="tooltiptext-after"/></a> 


<a href="../attribute/index.html#Package.Content.Application.Liability.ContinuingRepayment.PaymentType" class="tooltip-parent">Package.Content.Application.Liability.ContinuingRepayment.PaymentType<span class="tooltiptext-under">The type of repayment.</span> <span class="tooltiptext-after"/></a> 


<a href="../attribute/index.html#Package.Content.Application.Liability.ContinuingRepayment.RepaymentAmount" class="tooltip-parent">Package.Content.Application.Liability.ContinuingRepayment.RepaymentAmount<span class="tooltiptext-under">The repayment expense amount associated with repaying this liability.</span> <span class="tooltiptext-after"/></a> 


<a href="../attribute/index.html#Package.Content.Application.Liability.ContinuingRepayment.RepaymentFrequency" class="tooltip-parent">Package.Content.Application.Liability.ContinuingRepayment.RepaymentFrequency<span class="tooltiptext-under">The payment frequency of the repayment associated with this liability.</span> <span class="tooltiptext-after"/></a> 


<a href="../attribute/index.html#Package.Content.Application.Liability.ContinuingRepayment.TaxDeductible" class="tooltip-parent">Package.Content.Application.Liability.ContinuingRepayment.TaxDeductible<span class="tooltiptext-under">Indicates whether the expenses associated with this liability are tax deductible.</span> <span class="tooltiptext-after"/></a> 


<a href="../element/index.html#Package.Content.Application.Liability.PercentOwned.Owner" class="tooltip-parent">Package.Content.Application.Liability.PercentOwned.Owner<span class="tooltiptext-under">The details of the ownership of this liability, either as borrower or guarantor.</span> <span class="tooltiptext-after"/></a> 


<a href="../attribute/index.html#Package.Content.Application.Liability.PercentOwned.Owner.OwnerType" class="tooltip-parent">Package.Content.Application.Liability.PercentOwned.Owner.OwnerType<span class="tooltiptext-under">Describes whether the linked party is a borrower or guarantor on this liability.</span> <span class="tooltiptext-after"/></a> 


<a href="../attribute/index.html#Package.Content.Application.Liability.PercentOwned.Owner.Percent" class="tooltip-parent">Package.Content.Application.Liability.PercentOwned.Owner.Percent<span class="tooltiptext-under">Captures the percentage of the liability that the linked party is responsible for.</span> <span class="tooltiptext-after"/></a> 


<a href="../attribute/index.html#Package.Content.Application.Liability.PercentOwned.Owner.x_Party" class="tooltip-parent">Package.Content.Application.Liability.PercentOwned.Owner.x_Party<span class="tooltiptext-under">A reference to a party that is an responsible for the liability.</span> <span class="tooltiptext-after"/></a> 


<a href="../attribute/index.html#Package.Content.Application.Liability.PercentOwned.Proportions" class="tooltip-parent">Package.Content.Application.Liability.PercentOwned.Proportions<span class="tooltiptext-under">Flag to indicate whether the ownership proportions are equal across all owners, specified individually for each one, or not specified.</span> <span class="tooltiptext-after"/></a> 


<a href="../attribute/index.html#Package.Content.Application.Liability.Security.MortgagorDetails.ReasonForDifferentName" class="tooltip-parent">Package.Content.Application.Liability.Security.MortgagorDetails.ReasonForDifferentName<span class="tooltiptext-under">The reason the name of the mortgagor does not match the name on the certificate of title. Only applicable in Victoria.</span> <span class="tooltiptext-after"/></a> 


<a href="../attribute/index.html#Package.Content.Application.LoanDetails.FeaturesSelected.OffsetAccount.x_Account" class="tooltip-parent">Package.Content.Application.LoanDetails.FeaturesSelected.OffsetAccount.x_Account<span class="tooltiptext-under">A cross reference to an account (a Non Real Estate Asset element) to be used as the offset account.</span> <span class="tooltiptext-after"/></a> 

<br>

<a href="../attribute/index.html#addressType.Standard.LevelType" class="tooltip-parent">addressType.Standard.LevelType<span class="tooltiptext-under">The building level type of a standard address.</span> <span class="tooltiptext-after"/></a> 


<a href="../attribute/index.html#addressType.Standard.ToStreetNumber" class="tooltip-parent">addressType.Standard.ToStreetNumber<span class="tooltiptext-under">The upper bound of a range given for the street number of a house or property.</span> <span class="tooltiptext-after"/></a> 


<a href="../attribute/index.html#addressType.Standard.ToUnitNumber" class="tooltip-parent">addressType.Standard.ToUnitNumber<span class="tooltiptext-under">The upper bound of a range given for the unit number of a house or property.</span> <span class="tooltiptext-after"/></a> 


<a href="../attribute/index.html#addressType.Standard.UnitType" class="tooltip-parent">addressType.Standard.UnitType<span class="tooltiptext-under">The type of unit.</span> <span class="tooltiptext-after"/></a> 

<br>

<a href="../simpletype/index.html#levelTypeList" class="tooltip-parent">levelTypeList<span class="tooltiptext-under">levelTypeList</span> <span class="tooltiptext-after"/></a> 


- <a href="../simpletype/index.html#levelTypeList" class="tooltip-parent">levelTypeList.Basement<span class="tooltiptext-under">Basement</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#levelTypeList" class="tooltip-parent">levelTypeList.Floor<span class="tooltiptext-under">Floor</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#levelTypeList" class="tooltip-parent">levelTypeList.Ground<span class="tooltiptext-under">Ground</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#levelTypeList" class="tooltip-parent">levelTypeList.Level<span class="tooltiptext-under">Level</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#levelTypeList" class="tooltip-parent">levelTypeList.Lower Ground Floor<span class="tooltiptext-under">Lower Ground Floor</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#levelTypeList" class="tooltip-parent">levelTypeList.Lower Level<span class="tooltiptext-under">Lower Level</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#levelTypeList" class="tooltip-parent">levelTypeList.Mezzanine<span class="tooltiptext-under">Mezzanine</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#levelTypeList" class="tooltip-parent">levelTypeList.Observation Deck<span class="tooltiptext-under">Observation Deck</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#levelTypeList" class="tooltip-parent">levelTypeList.Parking<span class="tooltiptext-under">Parking</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#levelTypeList" class="tooltip-parent">levelTypeList.Penthouse<span class="tooltiptext-under">Penthouse</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#levelTypeList" class="tooltip-parent">levelTypeList.Platform<span class="tooltiptext-under">Platform</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#levelTypeList" class="tooltip-parent">levelTypeList.Podium<span class="tooltiptext-under">Podium</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#levelTypeList" class="tooltip-parent">levelTypeList.Rooftop<span class="tooltiptext-under">Rooftop</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#levelTypeList" class="tooltip-parent">levelTypeList.Sub-basement<span class="tooltiptext-under">Sub-basement</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#levelTypeList" class="tooltip-parent">levelTypeList.Upper Ground Floor<span class="tooltiptext-under">Upper Ground Floor</span> <span class="tooltiptext-after"/></a>


<a href="../simpletype/index.html#reasonForDifferentNameList" class="tooltip-parent">reasonForDifferentNameList<span class="tooltiptext-under">reasonForDifferentNameList</span> <span class="tooltiptext-after"/></a> 


- <a href="../simpletype/index.html#reasonForDifferentNameList" class="tooltip-parent">reasonForDifferentNameList.Abbreviation<span class="tooltiptext-under">Approved abbreviation.</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#reasonForDifferentNameList" class="tooltip-parent">reasonForDifferentNameList.Amalgamation<span class="tooltiptext-under">Amalgamation - Hospitals - Registered proprietor(s) may still exist.</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#reasonForDifferentNameList" class="tooltip-parent">reasonForDifferentNameList.Amalgamation Evidence<span class="tooltiptext-under">Amalgamation - Hospitals - Registered proprietor(s) may still exist - Registrar holds satisfactory evidence.</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#reasonForDifferentNameList" class="tooltip-parent">reasonForDifferentNameList.Change Incorporated Name<span class="tooltiptext-under">Change of incorporated name.</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#reasonForDifferentNameList" class="tooltip-parent">reasonForDifferentNameList.Deed Poll<span class="tooltiptext-under">Deed Poll.</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#reasonForDifferentNameList" class="tooltip-parent">reasonForDifferentNameList.Error in Register<span class="tooltiptext-under">Error in Register.</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#reasonForDifferentNameList" class="tooltip-parent">reasonForDifferentNameList.Legislative Change Company<span class="tooltiptext-under">Legislative change - Company - Registered proprietor(s) may still exist.</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#reasonForDifferentNameList" class="tooltip-parent">reasonForDifferentNameList.Legislative Change Company Evidence<span class="tooltiptext-under">Legislative change - Company - Registered proprietor(s) may still exist - Registrar holds satisfactory evidence.</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#reasonForDifferentNameList" class="tooltip-parent">reasonForDifferentNameList.Legislative Change Non Company<span class="tooltiptext-under">Legislative change - Non Company - Registered proprietor(s) may still exist.</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#reasonForDifferentNameList" class="tooltip-parent">reasonForDifferentNameList.Legislative Change Non Company Evidence<span class="tooltiptext-under">Legislative change - Non Company - Registered proprietor(s) may still exist - Registrar holds satisfactory evidence.</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#reasonForDifferentNameList" class="tooltip-parent">reasonForDifferentNameList.Marriage<span class="tooltiptext-under">Marriage.</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#reasonForDifferentNameList" class="tooltip-parent">reasonForDifferentNameList.Merger or Takeover<span class="tooltiptext-under">Merger or takeover - Registered proprietor(s) may still exist.</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#reasonForDifferentNameList" class="tooltip-parent">reasonForDifferentNameList.Merger or Takeover Evidence<span class="tooltiptext-under">Merger or takeover - Registered proprietor(s) may still exist - Registrar holds satisfactory evidence.</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#reasonForDifferentNameList" class="tooltip-parent">reasonForDifferentNameList.New Name<span class="tooltiptext-under">Adoption of new name.</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#reasonForDifferentNameList" class="tooltip-parent">reasonForDifferentNameList.Resumption of Maiden Name<span class="tooltiptext-under">Adoption of new name.</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#reasonForDifferentNameList" class="tooltip-parent">reasonForDifferentNameList.Sale or Transfer<span class="tooltiptext-under">Sale or transfer of enterprise - Registered Proprietor(s) may still exist.</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#reasonForDifferentNameList" class="tooltip-parent">reasonForDifferentNameList.Sale or Transfer Evidence<span class="tooltiptext-under">Sale or transfer of enterprise - Registered Proprietor(s) may still exist - Registrar holds satisfactory evidence.</span> <span class="tooltiptext-after"/></a>

<br>

- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Arterial<span class="tooltiptext-under">Arterial</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Banan<span class="tooltiptext-under">Banan</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Boardwalk<span class="tooltiptext-under">Boardwalk</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Boulevarde<span class="tooltiptext-under">Boulevarde</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Cluster<span class="tooltiptext-under">Cluster</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Connection<span class="tooltiptext-under">Connection</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Course<span class="tooltiptext-under">Course</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Dene<span class="tooltiptext-under">Dene</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Divide<span class="tooltiptext-under">Divide</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Dock<span class="tooltiptext-under">Dock</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Domain<span class="tooltiptext-under">Domain</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Firebreak<span class="tooltiptext-under">Firebreak</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Fireline<span class="tooltiptext-under">Fireline</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Firetrack<span class="tooltiptext-under">Firetrack</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Ford<span class="tooltiptext-under">Ford</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Gateway<span class="tooltiptext-under">Gateway</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Harbour<span class="tooltiptext-under">Harbour</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Heath<span class="tooltiptext-under">Heath</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Hollow<span class="tooltiptext-under">Hollow</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Keys<span class="tooltiptext-under">Keys</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Manor<span class="tooltiptext-under">Manor</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Outlet<span class="tooltiptext-under">Outlet</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Pursuit<span class="tooltiptext-under">Pursuit</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Return<span class="tooltiptext-under">Return</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Rising<span class="tooltiptext-under">Rising</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Shunt<span class="tooltiptext-under">Shunt</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Throughway<span class="tooltiptext-under">Throughway</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Twist<span class="tooltiptext-under">Twist</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Upper<span class="tooltiptext-under">Upper</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Waterway<span class="tooltiptext-under">Waterway</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#streetTypeList" class="tooltip-parent">streetTypeList.Woods<span class="tooltiptext-under">Woods</span> <span class="tooltiptext-after"/></a>


<a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList<span class="tooltiptext-under">unitTypeList</span> <span class="tooltiptext-after"/></a> 


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Antenna<span class="tooltiptext-under">Antenna</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Apartment<span class="tooltiptext-under">Apartment</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Automated Teller Machine<span class="tooltiptext-under">Automated Teller Machine</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Automatic Teller<span class="tooltiptext-under">Automatic Teller</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Barbecue<span class="tooltiptext-under">Barbecue</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Boatshed<span class="tooltiptext-under">Boatshed</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Building<span class="tooltiptext-under">Building</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Bungalow<span class="tooltiptext-under">Bungalow</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Cage<span class="tooltiptext-under">Cage</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Carpark<span class="tooltiptext-under">Carpark</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Carspace<span class="tooltiptext-under">Carspace</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Club<span class="tooltiptext-under">Club</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Condominium<span class="tooltiptext-under">Condominium</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Coolroom<span class="tooltiptext-under">Coolroom</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Cottage<span class="tooltiptext-under">Cottage</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Duplex<span class="tooltiptext-under">Duplex</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Factory<span class="tooltiptext-under">Factory</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Flat<span class="tooltiptext-under">Flat</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Garage<span class="tooltiptext-under">Garage</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Hall<span class="tooltiptext-under">Hall</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.House<span class="tooltiptext-under">House</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Kiosk<span class="tooltiptext-under">Kiosk</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Lease<span class="tooltiptext-under">Lease</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Lobby<span class="tooltiptext-under">Lobby</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Loft<span class="tooltiptext-under">Loft</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Lot<span class="tooltiptext-under">Lot</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Maisonette<span class="tooltiptext-under">Maisonette</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Marine Berth<span class="tooltiptext-under">Marine Berth</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Office<span class="tooltiptext-under">Office</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Penthouse<span class="tooltiptext-under">Penthouse</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Reserve<span class="tooltiptext-under">Reserve</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Room<span class="tooltiptext-under">Room</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Shed<span class="tooltiptext-under">Shed</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Shop<span class="tooltiptext-under">Shop</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Showroom<span class="tooltiptext-under">Showroom</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Sign<span class="tooltiptext-under">Sign</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Site<span class="tooltiptext-under">Site</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Stall<span class="tooltiptext-under">Stall</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Store<span class="tooltiptext-under">Store</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Strata Unit<span class="tooltiptext-under">Strata Unit</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Studio / Studio Apartment<span class="tooltiptext-under">Studio / Studio Apartment</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Substation<span class="tooltiptext-under">Substation</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Suite<span class="tooltiptext-under">Suite</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Tenancy<span class="tooltiptext-under">Tenancy</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Tower<span class="tooltiptext-under">Tower</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Townhouse<span class="tooltiptext-under">Townhouse</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Unit<span class="tooltiptext-under">Unit</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Vault<span class="tooltiptext-under">Vault</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Villa<span class="tooltiptext-under">Villa</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Ward<span class="tooltiptext-under">Ward</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Warehouse<span class="tooltiptext-under">Warehouse</span> <span class="tooltiptext-after"/></a>


- <a href="../simpletype/index.html#unitTypeList" class="tooltip-parent">unitTypeList.Workshop<span class="tooltiptext-under">Workshop</span> <span class="tooltiptext-after"/></a>


