<sup>This documentation was created the_current_time</sup>

![Logo](./img/lixi_logo_100.png)

### LIXI DAS-this_version_number_dots Overview

This documentation describes the LIXI Document Preparation and Settlements (DAS this_version_number_dots) Standard that was derived from the [LIXI Master Schema vmaster_version_number_dots](../../master/master_version_number_underscores/index.html) and supersedes all earlier versions. The announcement page for this release can be found on the LIXI web site: [LIXI Document Preparation and Settlements (DAS this_version_number_dots) Standard](https://lixi.org.au/lixi-standards/lixi-das-this_version_number_underscores-standard/).

### Purpose & Intended Audience

This documentation provides the data dictionary, glossary and detailed specifications that describe the LIXI Document Preparation and Settlements (DAS this_version_number_dots) Standard. The intended audience members are those Licensees or Members that are planning to implement, or have already implemented a version of a LIXI Document Preparation and Settlements (DAS 2.x).

### Release Contents

All the components of this release are available to Licensees and Members in the secure area of the LIXI Website [here](https://lixi.org.au/lixi-standards/lixi-das-this_version_number_underscores-standard/).

### Root Element

The Root Element of a LIXI conformant DAS Message is the '[Package](../this_version_number_underscores/element/index.html#Package)' Element.

### Change Management

The source code for the LIXI Standards are now hosted in [LIXILab](https://standards.lixi.org.au/) (LIXI's GitLab Repository). Please contact LIXI if you would like access. To get further information on the Change Management Processes for this standard, or to request a change, please visit the Change Management page on the LIXI website [here](http://lixi.org.au/dc-cal-2-0-change-request/).

### Statement of Proprietary Information

This standard is confidential to LIXI Limited and may not be disclosed, duplicated, or used for any purpose, in whole or in part, without the prior consent of LIXI Limited. Both the XML Schema Definition files and associated documentation are licensed under the LIXI End User Licence Agreement (EULA) that can be found here: [LIXI-End-User-Licence-Agreement](https://lixi.org.au/assets/LIXI-End-User-Licence-Agreement-2017-11.pdf). An introduction to this EULA is available here: [Introduction to EULA 2013 10 V.2.pdf](https://cdn.lixi.org.au/assets/Introduction-to-EULA-2013-10-V.2.pdf).