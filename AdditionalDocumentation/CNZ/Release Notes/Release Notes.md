<a name="Index"></a>
<br>
### Index of Changes
<a name="Enhancements"></a>
<br>
#### Enhancements 
 <a href="#353" title="Deposit Account Feature Selection for Specific Applicants">Deposit Account Feature Selection for Specific Applicants</a> (<a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/353" title="LIXILab Issue 353">Ticket</a>)<br>
<a href="#354" title="Update ABS Lending Purpose Code List">Update ABS Lending Purpose Code List</a> (<a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/354" title="LIXILab Issue 354">Ticket</a>)<br>
<a href="#355" title="Allow Application Type 'Deposit Account' to be Captured">Allow Application Type 'Deposit Account' to be Captured</a> (<a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/355" title="LIXILab Issue 355">Ticket</a>)<br>
<a href="#357" title="Allow the origination date of a liability to be captured">Allow the origination date of a liability to be captured</a> (<a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/357" title="LIXILab Issue 357">Ticket</a>)<br>
<a href="#358" title="Allow the end date of the interest only period of a liability to be captured">Allow the end date of the interest only period of a liability to be captured</a> (<a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/358" title="LIXILab Issue 358">Ticket</a>)<br>
<a href="#362" title="Allow Company Financials to be captured for Self Employed Applicants">Allow Company Financials to be captured for Self Employed Applicants</a> (<a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/362" title="LIXILab Issue 362">Ticket</a>)<br>
<a href="#363" title="Allow Living Expense and Other Commitments Frequency to capture all required 'frequency' values">Allow Living Expense and Other Commitments Frequency to capture all required 'frequency' values</a> (<a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/363" title="LIXILab Issue 363">Ticket</a>)<br>
<a href="#366" title="Allow capture of additional Company Financial details">Allow capture of additional Company Financial details</a> (<a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/366" title="LIXILab Issue 366">Ticket</a>)<br>
<a href="#373" title="Events to indicate Additional Information Requested and Supporting Documents Events">Events to indicate Additional Information Requested and Supporting Documents Events</a> (<a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/373" title="LIXILab Issue 373">Ticket</a>)<br>
<a href="#375" title="Allow a status that indicates a message has reverted to the data capture stage">Allow a status that indicates a message has reverted to the data capture stage</a> (<a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/375" title="LIXILab Issue 375">Ticket</a>)<br>
<a href="#381" title="Allow capture of Branch phone and fax numbers">Allow capture of Branch phone and fax numbers</a> (<a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/381" title="LIXILab Issue 381">Ticket</a>)<br>
<a href="#382" title="Start Date, Duration, and End Date (Where Applicable) for Company and Trust Applicant Addresses">Start Date, Duration, and End Date (Where Applicable) for Company and Trust Applicant Addresses</a> (<a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/382" title="LIXILab Issue 382">Ticket</a>)<br>
<a href="#383" title="Capture the Beneficial Owner of a  Company Applicant or Trust Applicant">Capture the Beneficial Owner of a  Company Applicant or Trust Applicant</a> (<a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/383" title="LIXILab Issue 383">Ticket</a>)<br>
<br>
#### Annotation Changes 
 <p>These issues exclusively involve changes to schema elements within annotation elements (documentation or labels). </p> <br><a href="#333" title="The element PropertySearchValidation requires a definition">The element PropertySearchValidation requires a definition</a> (<a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/333" title="LIXILab Issue 333">Ticket</a>)<br>
<a href="#378" title="PAYG and PAYE Income amount definitions need updating to explicitly declare if they are gross or net.">PAYG and PAYE Income amount definitions need updating to explicitly declare if they are gr ... </a> (<a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/378" title="LIXILab Issue 378">Ticket</a>)<br>
<br>
#### Structural Changes 
 <p>These issues exclusively involve changes to schema structure that is non breaking such as enumeration sort order etc. </p> <br><a href="#371" title="Add definitions for the lists of preferred contact methods (person contact and company contact lists)">Add definitions for the lists of preferred contact methods (person contact and company con ... </a> (<a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/371" title="LIXILab Issue 371">Ticket</a>)<br>
<hr /> 
#### Details of Changes 
 <a name="353"></a> <br>
<h4>Deposit Account Feature Selection for Specific Applicants </h4> <br>
<h5>Problem / Requirement Statement</h5>

<p>The schema should allow a different selection of account features to be captured for each Deposit Account applicant. </p>

<p>A deposit account may have more than one applicant associated with it. Product features can be selected for each Account (e.g. request a deposit book). Currently, only one set of features can be selected for each account. Instead, the schema should allow a set of selected features for each applicant.</p>

<p>Also, some new features are required: </p>

<ul>
<li>the applicant is allowed to request EFTPOS card</li>
<li>the applicant can select methods of statement delivery (email or paper)</li>
</ul>

<p><br></p>

<h5>Solution Overview</h5>

<p>Modify the Features Selected element to be repeatable. </p>

<p>Add a cross reference to associate each Features Selected element with an applicant. </p>

<ul>
<li>Definition: "The applicant that has selected these features (when this attribute is not populated, the selected features apply to the entire account)."</li>
</ul>

<p>Add attributes for the new account features: </p>

<ul>
<li>Statement Delivery by Email</li>
<li>Statement Delivery by Paper Mail</li>
<li>Request an EFTPOS Card</li>
</ul>

<p>Update the definition of Features Selected to:</p>

<ul>
<li>"Contains information about the product features requested by the applicant or applicants."</li>
</ul>

<p><br></p>

<h5>Solution Detail</h5>

<p>Change maxOccurs from "1" to "unbounded" for the elements:</p>

<ul>
<li>Package.Content.Application.DepositAccountDetails.FeaturesSelected</li>
</ul>

<p>To associate the features with an applicant, add new attributes:</p>

<ul>
<li>Package.Content.Application.DepositAccountDetails.FeaturesSelected.x_Applicant (uniqueIDType)</li>
</ul>

<p>Also, add new attributes:</p>

<ul>
<li>Package.Content.Application.DepositAccountDetails.FeaturesSelected.StatementEmailDelivery (yesNoList)</li>
<li>Package.Content.Application.DepositAccountDetails.FeaturesSelected.StatementPaperDelivery (yesNoList)</li>
<li>Package.Content.Application.DepositAccountDetails.FeaturesSelected.EFTPOSCard (yesNoList) </li>
</ul>

<p><br></p>

<h5>Related Links</h5>

<ul>
<li><a href="https://smedia.lixi.org.au/standards-docs.html?standard=master&amp;version=current&amp;component=element&amp;item=Package.Content.Application.DepositAccountDetails.FeaturesSelected">Latest documentation for element FeaturesSelected in Master</a></li>
<li>Git issue: <a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/353">Issue 353 in LIXI2 / TransactionMaster</a></li>
</ul>
<em><a href="#Index">Return To Index</a></em><br><br>
<hr /><a name="354"></a> <br>
<h4>Update ABS Lending Purpose Code List </h4> <br>
<h5>Problem / Requirement Statement</h5>

<p>ABS Lending Purpose Code List requires updating. The following codes are required:</p>

<ul>
<li>233 Unsecured loans for alterations and/or additions to existing dwellings</li>
<li>235 Unsecured loans to assist with the purchase or construction of housing for owner</li>
</ul>

<p><br></p>

<h5>Solution Overview</h5>

<p>Update the absLendingPurposeCodeList to include 233 and 235</p>

<p><br></p>

<h5>Solution Detail</h5>

<p>Add new enumerations:</p>

<ul>
<li>absLendingPurposeCodeList.ABS-233</li>
<li>absLendingPurposeCodeList.ABS-235</li>
</ul>

<p><br></p>

<h5>Related Links</h5>

<ul>
<li><a href="https://smedia.lixi.org.au/standards-docs.html?standard=master&amp;version=current&amp;component=simpletype&amp;item=abslendingpurposecodelist-simpletype">Latest documentation for the simple type abslendingpurposecodelist</a></li>
<li>Git issue: <a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/354">Issue 354 in LIXI2 / TransactionMaster</a></li>
</ul>
<em><a href="#Index">Return To Index</a></em><br><br>
<hr /><a name="355"></a> <br>
<h4>Allow Application Type 'Deposit Account' to be Captured </h4> <br>
<h5>Problem / Requirement Statement</h5>

<p>Some applications that are being sent to systems using LIXI standard are deposit account applications. This information needs to be captured.</p>

<p><br></p>

<h5>Solution Overview</h5>

<p>Add an enumeration to applicationTypeList to capture the type: Deposit Account.</p>

<p><br></p>

<h5>Solution Detail</h5>

<p>Add Enumeration</p>

<ul>
<li>applicationTypeList.Deposit Account</li>
</ul>

<p><br></p>

<h5>Related Links</h5>

<ul>
<li><a href="https://smedia.lixi.org.au/standards-docs.html?standard=master&amp;version=current&amp;component=simpletype&amp;item=applicationtypelist-simpletype">Latest documentation for simple type applicationTypeList in Master</a></li>
<li>Git issue: <a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/355">Issue 355 in LIXI2 / TransactionMaster</a></li>
</ul>
<em><a href="#Index">Return To Index</a></em><br><br>
<hr /><a name="357"></a> <br>
<h4>Allow the origination date of a liability to be captured </h4> <br>
<h5>Problem / Requirement Statement</h5>

<p>The origination date of a liability needs to be captured. The origination date is to be used for serviceability purposes - to calculate the repayment amount of the liability.</p>

<p><br></p>

<h5>Solution Overview</h5>

<p>Add new attribute</p>

<ul>
<li>Package.Content.Application.Liability.OriginationDate</li>
</ul>

<p><br></p>

<h5>Solution Detail</h5>

<p>Attribute Data Type - dateType</p>

<p>Attribute Use - Optional</p>

<p>Attribute Definition - "The origination date of the liability"</p>

<p><br></p>

<h5>Related Links</h5>

<ul>
<li><a href="https://smedia.lixi.org.au/standards-docs.html?standard=master&amp;version=current&amp;component=element&amp;item=Package.Content.Application.Liability">Latest documentation for element Package.Content.Application.Liability in Master</a></li>
<li>Git issue: <a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/357">Issue 357 in LIXI2 / TransactionMaster</a></li>
</ul>
<em><a href="#Index">Return To Index</a></em><br><br>
<hr /><a name="358"></a> <br>
<h4>Allow the end date of the interest only period of a liability to be captured </h4> <br>
<h5>Problem / Requirement Statement</h5>

<p>Allow the end date of the interest only period of a liability to be captured. For Liabilities where OriginalTerm PaymentType is 'Interest Only', allow capture of the specific date when the interest only period ends. This attribute is used for serviceability purposes - to calculate the repayment amount of the liability.</p>

<p><br></p>

<h5>Solution Overview</h5>

<p>Add an attribute to capture the Interest Only End Date.</p>

<p><br></p>

<h5>Solution Detail</h5>

<p>Add the Attribute:</p>

<ul>
<li>Package.Content.Application.Liability.OriginalTerm.InterestOnlyEndDate</li>
</ul>

<p><br></p>

<h5>Related Links</h5>

<ul>
<li><a href="https://smedia.lixi.org.au/standards-docs.html?standard=master&amp;version=current&amp;component=element&amp;item=Package.Content.Application.Liability.OriginalTerm">Latest documentation for the element OriginalTerm</a></li>
<li>Git issue: <a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/358">Issue 358 in LIXI2 / TransactionMaster</a></li>
</ul>
<em><a href="#Index">Return To Index</a></em><br><br>
<hr /><a name="362"></a> <br>
<h4>Allow Company Financials to be captured for Self Employed Applicants </h4> <br>
<h5>Problem / Requirement Statement</h5>

<p>Company Financials need to be captured for Self Employed Applicants. The Company Financials can be captured for company applicants, Trust Applicants, and NonRealEstateAssets. A means to capture Company Financials for Self Employed Applicants needs to be added.</p>

<p><br></p>

<h5>Solution Overview</h5>

<p>Add a FinancialAnalysis element under SelfEmployed</p>

<p><br></p>

<h5>Solution Detail</h5>

<p>Add new Elements:</p>

<ul>
<li>Package.Content.Application.PersonApplicant.Employment.SelfEmployed.FinancialAnalysis</li>
<li>Package.Content.Application.PersonApplicant.Employment.SelfEmployed.FinancialAnalysis.CompanyFinancials</li>
</ul>

<p>Add new Attributes:</p>

<ul>
<li>Package.Content.Application.PersonApplicant.Employment.SelfEmployed.FinancialAnalysis.AnnualPaymentOnCommitments</li>
<li>Package.Content.Application.PersonApplicant.Employment.SelfEmployed.FinancialAnalysis.CompleteFinancialAnalysis</li>
<li>Package.Content.Application.PersonApplicant.Employment.SelfEmployed.FinancialAnalysis.CompanyFinancials.x_CompanyFinancials</li>
</ul>

<p><br></p>

<h5>Related Links</h5>

<ul>
<li><a href="https://smedia.lixi.org.au/standards-docs.html?standard=master&amp;version=current&amp;component=element&amp;item=Package.Content.Application.PersonApplicant.Employment.SelfEmployed">Latest documentation for element SelfEmployed in Master</a></li>
<li>Git issue: <a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/362">Issue 362 in LIXI2 / TransactionMaster</a></li>
</ul>
<em><a href="#Index">Return To Index</a></em><br><br>
<hr /><a name="363"></a> <br>
<h4>Allow Living Expense and Other Commitments Frequency to capture all required 'frequency' values </h4> <br>
<h5>Problem / Requirement Statement</h5>

<p>For Household expenses (Living Expenses and Other Commitments), "Frequency" attribute is using the "frequencyShortList" which restricts the broker and applicants to select other types of frequencies for their expenses payments. For example, most of the Rates Notice and property utilities (gas and electricity) are paid quarterly, but this option is not available in the "frequencyShortList".</p>

<p><br></p>

<h5>Solution Overview</h5>

<p>Change the frequency list that is currently being used for Living Expenses and Other Commitments.</p>

<p><br></p>

<h5>Solution Detail</h5>

<p>Change the type from frequencyShortList to frequencyFullList for the attributes:</p>

<ul>
<li>Package.Content.Application.Household.ExpenseDetails.LivingExpense.Frequency</li>
<li>Package.Content.Application.Household.ExpenseDetails.OtherCommitment.Frequency</li>
</ul>

<p><br></p>

<h5>Related Links</h5>

<ul>
<li><a href="https://smedia.lixi.org.au/standards-docs.html?standard=master&amp;version=current&amp;component=attribute&amp;item=Package.Content.Application.Household.ExpenseDetails.LivingExpense.Frequency">Latest documentation for attribute Frequency in LivingExpense</a></li>
<li><a href="https://smedia.lixi.org.au/standards-docs.html?standard=master&amp;version=current&amp;component=attribute&amp;item=Package.Content.Application.Household.ExpenseDetails.OtherCommitment.Frequency">Latest documentation for attribute Frequency in OtherCommitment</a></li>
<li><a href="https://smedia.lixi.org.au/standards-docs.html?standard=master&amp;version=current&amp;component=simpletype&amp;item=frequencyFullList">Latest documentation for simpleType frequencyFullList in Master</a></li>
<li>Git issue: <a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/363">Issue 363 in LIXI2 / TransactionMaster</a></li>
</ul>
<em><a href="#Index">Return To Index</a></em><br><br>
<hr /><a name="366"></a> <br>
<h4>Allow capture of additional Company Financial details </h4> <br>
<h5>Problem / Requirement Statement</h5>

<p>Capture of additional Company Financial details is required to assist in making an accurate credit decision.</p>

<p>The Cost of Goods Sold is used in the calculation of the Gross Profit Margin for a Business.</p>

<p>The Total Expenses is used in the calculation of the Expense to Sales Ratio for a Business.</p>

<p>The Equity and Shareholder Funds is used in the calculation of the Equity for a Business.</p>

<p><br></p>

<h5>Solution Overview</h5>

<p>Add CostOfGoodsSold, TotalExpenses, Equity and ShareholderFunds to the CompanyFinancials element.</p>

<p><br></p>

<h5>Solution Detail</h5>

<p>Add Attributes:</p>

<ul>
<li>Package.Content.Application.CompanyFinancials.ProfitAndLoss.CostOfGoodsSold</li>
<li>Package.Content.Application.CompanyFinancials.ProfitAndLoss.TotalExpenses</li>
<li>Package.Content.Application.CompanyFinancials.BalanceSheet.Equity</li>
<li>Package.Content.Application.CompanyFinancials.BalanceSheet.ShareholderFunds</li>
</ul>

<p><br></p>

<h5>Related Links</h5>

<ul>
<li><p><a href="https://smedia.lixi.org.au/standards-docs.html?standard=master&amp;version=current&amp;component=element&amp;item=Package.Content.Application.CompanyFinancials.ProfitAndLoss">Latest documentation for element ProfitAndLoss in Master</a></p></li>
<li><p><a href="https://smedia.lixi.org.au/standards-docs.html?standard=master&amp;version=current&amp;component=element&amp;item=Package.Content.Application.CompanyFinancials.BalanceSheet">Latest documentation for element BalanceSheet in Master</a></p></li>
<li>Git issue: <a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/366">Issue 366 in LIXI2 / TransactionMaster</a></li>
</ul>
<em><a href="#Index">Return To Index</a></em><br><br>
<hr /><a name="373"></a> <br>
<h4>Events to indicate Additional Information Requested and Supporting Documents Events </h4> <br>
<h5>Problem / Requirement Statement</h5>

<p>Events required for backchannel implementation are currently not covered by the standard.</p>

<p>The following values have been identified as missing:</p>

<ul>
<li><p>Additional Information Has Been Requested (Definition: Application has already been registered but additional explanations and/or comments are required from the applicants or loan writer in order to complete the assessment.)</p></li>
<li><p>Supporting Documents Determined to Be Incomplete (Definition: The supporting documents provided by the customer do not cover the checklist requirements, additional supporting documents are expected.) NOTE: "Define Incomplete"</p></li>
<li><p>Supporting Documents Have Been Verified (Definition: The customer provided supporting documents have been verified.) </p></li>
<li><p>Awaiting Supporting Documents (Definition: The customer is now required to provide supporting documents.)</p></li>
</ul>

<p>Please add these as enumerations under the eventNameApplicationInstructionsList for CAL and CNZ.</p>

<p><br></p>

<h5>Solution Overview</h5>

<p>Add new enumerations to eventNameApplicationInstructionsList.</p>

<p><br></p>

<h5>Solution Detail</h5>

<p>Add enumerations:</p>

<ul>
<li>eventNameApplicationInstructionsList.Additional Information Has Been Requested</li>
<li>eventNameApplicationInstructionsList.Supporting Documents Determined to Be Incomplete</li>
<li>eventNameApplicationInstructionsList.Supporting Documents Have Been Verified</li>
<li>eventNameApplicationInstructionsList.Awaiting Supporting Documents</li>
</ul>

<p><br></p>

<h5>Related Links</h5>

<ul>
<li><a href="https://smedia.lixi.org.au/standards-docs.html?standard=master&amp;version=current&amp;component=simpletype&amp;item=eventNameApplicationInstructionsList">Latest documentation for simpleType eventNameApplicationInstructionsList in Master</a></li>
<li>Git issue: <a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/373">Issue 373 in LIXI2 / TransactionMaster</a></li>
</ul>
<em><a href="#Index">Return To Index</a></em><br><br>
<hr /><a name="375"></a> <br>
<h4>Allow a status that indicates a message has reverted to the data capture stage </h4> <br>
<h5>Problem / Requirement Statement</h5>

<p>A LIXI message requires the capacity to indicate that the message has incomplete or incorrect data and has been reverted to the data capture stage. The status type needs to indicate that the application has reached a point where it has been determined that the application does not contain the necessary data to continue processing. The data may be either incomplete or incorrect. At this point the application will be reverted to the data capture stage to correct or complete the data capture.</p>

<p><br></p>

<h5>Solution Overview</h5>

<p>Add an enumeration to the statusNameApplicationInstructionsList to indicate that the application has reverted to the data capture stage. </p>

<p><br></p>

<h5>Solution Detail</h5>

<p>Add enumeration:</p>

<ul>
<li>statusNameApplicationInstructionsList.Reverted to Capture</li>
</ul>

<p><br></p>

<h5>Related Links</h5>

<ul>
<li><a href="https://smedia.lixi.org.au/standards-docs.html?standard=master&amp;version=current&amp;component=simpletype&amp;item=statusNameApplicationInstructionsList">Latest documentation for simpleType statusNameApplicationInstructionsList in Master</a></li>
<li>Git issue: <a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/375">Issue 375 in LIXI2 / TransactionMaster</a></li>
</ul>
<em><a href="#Index">Return To Index</a></em><br><br>
<hr /><a name="381"></a> <br>
<h4>Allow capture of Branch phone and fax numbers </h4> <br>
<h5>Problem / Requirement Statement</h5>

<p>Ability to capture phone and fax details of Branches</p>

<p><br></p>

<h5>Solution Overview</h5>

<p>Add a "Contact" child element to the "BranchDomicile" and "BranchSign" elements. 
The Contact elements will have child elements "OfficePhone" and "OfficeFax".</p>

<p><br></p>

<h5>Solution Detail</h5>

<p>Add the Elements:</p>

<ul>
<li>Package.Content.Application.Overview.BranchDomicile.Contact</li>
<li>Package.Content.Application.Overview.BranchDomicile.Contact.OfficePhone</li>
<li>Package.Content.Application.Overview.BranchDomicile.Contact.OfficeFax</li>
<li>Package.Content.Application.Overview.BranchSign.Contact</li>
<li>Package.Content.Application.Overview.BranchSign.Contact.OfficePhone</li>
<li>Package.Content.Application.Overview.BranchSign.Contact.OfficeFax</li>
</ul>

<p><br></p>

<h5>Related Links</h5>

<ul>
<li><a href="https://smedia.lixi.org.au/standards-docs/cal/2_6_3/element/index.html#Package.Content.Application.Overview.BranchSign">Latest documentation for element BranchSign in Master</a></li>
<li><a href="https://smedia.lixi.org.au/standards-docs/cal/2_6_3/element/index.html#Package.Content.Application.Overview.BranchDomicile">Latest documentation for element BranchDomicile in Master</a></li>
<li><a href="https://smedia.lixi.org.au/standards-docs/cal/2_6_3/element/index.html#financialAccountType.BranchDomicile">Latest documentation for simpleType financialAccountType in Master</a></li>
<li>Git issue: <a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/381">Issue 381 in LIXI2 / TransactionMaster</a></li>
</ul>
<em><a href="#Index">Return To Index</a></em><br><br>
<hr /><a name="382"></a> <br>
<h4>Start Date, Duration, and End Date (Where Applicable) for Company and Trust Applicant Addresses </h4> <br>
<h5>Problem / Requirement Statement</h5>

<p>Most lenders require the duration of each address added for Person, Company and Trust applicants. Currently we only have the ability to collect the duration for addresses under Person Applicants. There are no Start/End Date and/or Duration fields available for Company or Trust applicants.</p>

<p><br></p>

<h5>Solution Overview</h5>

<p>Add new attributes to capture the start and end dates for addresses.</p>

<p>Add new elements to capture the duration for addresses.</p>

<p><br></p>

<h5>Solution Detail</h5>

<p>Add the elements:</p>

<ul>
<li>Package.Content.Application.CompanyApplicant.Contact.PreviousRegisteredAddressDuration</li>
<li>Package.Content.Application.CompanyApplicant.Contact.PrincipalTradingAddressDuration</li>
<li>Package.Content.Application.CompanyApplicant.Contact.RegisteredAddressDuration</li>
<li>Package.Content.Application.TrustApplicant.Contact.PrincipalTradingAddressDuration</li>
<li>Package.Content.Application.TrustApplicant.Contact.RegisteredAddressDuration</li>
</ul>

<p>Add the attributes:</p>

<ul>
<li>Package.Content.Application.CompanyApplicant.Contact.PreviousRegisteredAddressEndDate</li>
<li>Package.Content.Application.CompanyApplicant.Contact.PreviousRegisteredAddressStartDate</li>
<li>Package.Content.Application.CompanyApplicant.Contact.PrincipalTradingAddressStartDate</li>
<li>Package.Content.Application.CompanyApplicant.Contact.RegisteredAddressStartDate</li>
<li>Package.Content.Application.TrustApplicant.Contact.PrincipalTradingAddressStartDate</li>
<li>Package.Content.Application.TrustApplicant.Contact.RegisteredAddressStartDate</li>
</ul>

<p><br></p>

<h5>Related Links</h5>

<ul>
<li><p><a href="https://smedia.lixi.org.au/standards-docs.html?standard=master&amp;version=current&amp;component=element&amp;item=Package.Content.Application.CompanyApplicant.Contact">Latest documentation for element Contact (element in CompanyApplicant) in Master</a></p></li>
<li><p><a href="https://smedia.lixi.org.au/standards-docs.html?standard=master&amp;version=current&amp;component=attribute&amp;item=Package.Content.Application.CompanyApplicant.Contact.x_RegisteredAddress">Latest documentation for attribute x_RegisteredAddress (attribute in Contact in CompanyApplicant) in Master</a></p></li>
<li><p><a href="https://smedia.lixi.org.au/standards-docs.html?standard=master&amp;version=current&amp;component=attribute&amp;item=Package.Content.Application.CompanyApplicant.Contact.x_PrincipalTradingAddress">Latest documentation for attribute x_PrincipalTradingAddress (attribute in Contact in CompanyApplicant) in Master</a></p></li>
<li><p><a href="https://smedia.lixi.org.au/standards-docs.html?standard=master&amp;version=current&amp;component=attribute&amp;item=Package.Content.Application.CompanyApplicant.Contact.x_PreviousRegisteredAddress">Latest documentation for attribute x_PreviousRegisteredAddress (attribute) in Master</a></p></li>
<li><p><a href="https://smedia.lixi.org.au/standards-docs.html?standard=master&amp;version=current&amp;component=element&amp;item=Package.Content.Application.TrustApplicant.Contact">Latest documentation for element Contact (element in TrustApplicant) in Master</a></p></li>
<li><p><a href="https://smedia.lixi.org.au/standards-docs.html?standard=master&amp;version=current&amp;component=attribute&amp;item=Package.Content.Application.TrustApplicant.Contact.x_RegisteredAddress">Latest documentation for attribute x_RegisteredAddress (attribute in Contact in TrustApplicant) in Master</a></p></li>
<li><p><a href="https://smedia.lixi.org.au/standards-docs.html?standard=master&amp;version=current&amp;component=attribute&amp;item=Package.Content.Application.TrustApplicant.Contact.x_PrincipalTradingAddress">Latest documentation for attribute x_PrincipalTradingAddress (attribute in Contact in TrustApplicant) in Master</a></p></li>
<li>Git issue: <a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/382">Issue 382 in LIXI2 / TransactionMaster</a></li>
</ul>
<em><a href="#Index">Return To Index</a></em><br><br>
<hr /><a name="383"></a> <br>
<h4>Capture the Beneficial Owner of a  Company Applicant or Trust Applicant </h4> <br>
<h5>Problem / Requirement Statement</h5>

<p>Under the AML/CFT Act of 2009 lenders are required to identify the beneficial owners the applicant entity.</p>

<p>Beneficial owners can be anyone:</p>

<ul>
<li>Who owns more than 25% of the applicant entity.</li>
<li>Who has effective control of the applicant entity.</li>
<li>On whose behalf the applicant entity is conducting a transaction</li>
</ul>

<p><br></p>

<h5>Solution Overview</h5>

<p>Add a new element to capture the beneficial owner of a Company Applicant, that associates a Company Applicant with a Related Person.</p>

<p>Add a new element to capture the beneficial owner of a Trust Applicant, that associates a Trust Applicant with a Related Person.</p>

<p><br></p>

<h5>Solution Detail</h5>

<p>Add the new Elements:</p>

<ul>
<li>Package.Content.Application.CompanyApplicant.BeneficialOwner</li>
<li>Package.Content.Application.TrustApplicant.BeneficialOwner</li>
</ul>

<p>Add the new Attibutes:</p>

<ul>
<li>Package.Content.Application.CompanyApplicant.BeneficialOwner.x_BeneficialOwner</li>
<li>Package.Content.Application.TrustApplicant.BeneficialOwner.x_BeneficialOwner</li>
</ul>

<p><br></p>

<h5>Related Links</h5>

<ul>
<li><a href="https://smedia.lixi.org.au/standards-docs.html?standard=master&amp;version=current&amp;component=element&amp;item=Package.Content.Application.CompanyApplicant">Latest documentation for element Package.Content.Application.CompanyApplicant in Master</a></li>
<li><a href="https://smedia.lixi.org.au/standards-docs.html?standard=master&amp;version=current&amp;component=element&amp;item=Package.Content.Application.TrustApplicant">Latest documentation for element Package.Content.Application.TrustApplicant in Master</a></li>
<li>Git issue: <a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/383">Issue 383 in LIXI2 / TransactionMaster</a></li>
</ul>
<em><a href="#Index">Return To Index</a></em><br><br>
<hr /><a name="333"></a> <br>
<h4>The element PropertySearchValidation requires a definition </h4> <br>
<h5>Problem / Requirement Statement</h5>

<p>The definition of the element 'Package.Content.Application.RealEstateAsset.PropertySearchValidation' requires updating.</p>

<p><br></p>

<h5>Solution Overview</h5>

<p>Update definition of element PropertySearchValidation.</p>

<p><br></p>

<h5>Solution Detail</h5>

<p>Update definition of 'Package.Content.Application.RealEstateAsset.PropertySearchValidation' to:</p>

<p>'The results returned by a third party property search service. These services can return reports for clients, value estimates, and other property data for the specified property address. Lenders may base their decision whether to order or skip a full valuation report using the data returned by the search service.'</p>

<p><br></p>

<h5>Related Links</h5>

<ul>
<li><a href="https://smedia.lixi.org.au/standards-docs.html?standard=master&amp;version=current&amp;component=element&amp;item=Package.Content.Application.RealEstateAsset.PropertySearchValidation">Latest documentation for element Package.Content.Application.RealEstateAsset.PropertySearchValidation in Master</a></li>
<li>Git issue: <a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/333">Issue 333 in LIXI2 / TransactionMaster</a></li>
</ul>
<em><a href="#Index">Return To Index</a></em><br><br>
<hr /><a name="378"></a> <br>
<h4>PAYG and PAYE Income amount definitions need updating to explicitly declare if they are gross or net. </h4> <br>
<h5>Problem / Requirement Statement</h5>

<p>Some PAYG &amp; PAYE income amounts (and probably other amounts) need definitions to be updated to explicitly declare if they are gross or net amounts. Also make the definitions consistent with respect to passive/active voice and eliminate the use of the term 'Captures the'.</p>

<p><br></p>

<h5>Solution Overview</h5>

<p>Update 'amount' definitions where there could be ambiguity as to whether they are gross or net.
Update all definitions to remove the use of the term 'Captures the', and make definitions consistent with respect to passive/active voice.</p>

<p><br></p>

<h5>Related Links</h5>

<ul>
<li><a href="https://smedia.lixi.org.au/standards-docs/master/2_2_3/element/index.html#Package.Content.Application.PersonApplicant.Employment.PAYG.Income">Latest documentation for PAYG.Income in Master 2.2.3</a></li>
<li><a href="https://smedia.lixi.org.au/standards-docs/master/2_2_3/element/index.html#Package.Content.Application.PersonApplicant.Employment.PAYE.Income">Latest documentation for PAYE.Income in Master 2.2.3</a></li>
<li>Git issue: <a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/378">Issue 378 in LIXI2 / TransactionMaster</a></li>
</ul>
<em><a href="#Index">Return To Index</a></em><br><br>
<hr /><a name="371"></a> <br>
<h4>Add definitions for the lists of preferred contact methods (person contact and company contact lists) </h4> <br>
<h5>Problem / Requirement Statement</h5>

<p>Add definitions for the lists of preferred contact methods. </p>

<p><br></p>

<h5>Solution Overview</h5>

<p>Add definitions for the SimpleTypes 'PreferredContactPersonList' and 'PreferredContactMethodList'</p>

<p><br></p>

<h5>Solution Detail</h5>

<p>PreferredContactPersonList Definition:  </p>

<ul>
<li>"A list of methods to contact a person."</li>
</ul>

<p>PreferredContactCompanyList Definition:  </p>

<ul>
<li>"A list of methods to contact a company."</li>
</ul>

<p><br></p>

<h5>Related Links</h5>

<ul>
<li><p><a href="https://smedia.lixi.org.au/standards-docs.html?standard=master&amp;version=current&amp;component=simpletype&amp;item=preferredContactPersonList">Latest documentation for simpleType preferredContactPersonList in Master</a></p></li>
<li><p><a href="https://smedia.lixi.org.au/standards-docs.html?standard=master&amp;version=current&amp;component=simpletype&amp;item=preferredContactMethodList">Latest documentation for simpleType preferredContactMethodList in Master</a></p></li>
<li>Git issue: <a href="https://standards.lixi.org.au/lixi2/TransactionMaster/issues/371">Issue 371 in LIXI2 / TransactionMaster</a></li>
</ul>
<em><a href="#Index">Return To Index</a></em><br><br>
<hr />