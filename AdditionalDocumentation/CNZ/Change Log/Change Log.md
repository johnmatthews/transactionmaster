![Logo](./img/lixi_logo_100.png)


### New In LIXI CNZ.2.0.7

This section contains a list of the new items that have been added to LIXI CNZ.2.0.6 to create LIXI CNZ.2.0.7.<br><br>
<a href="../attribute/index.html#Package.Content.Application.Liability.ClearingFromOtherSourceAmount" class="tooltip-parent">Package.Content.Application.Liability.ClearingFromOtherSourceAmount<span class="tooltiptext-under">Captures the exact amount that will be refinanced for this liability using funds from a source that is not the new loan.</span> <span class="tooltiptext-after"/></a> 


<a href="../attribute/index.html#Package.Content.Application.Liability.ClearingFromThisLoanAmount" class="tooltip-parent">Package.Content.Application.Liability.ClearingFromThisLoanAmount<span class="tooltiptext-under">Captures the exact amount that will be refinanced for this liability using funds sourced from a new loan.</span> <span class="tooltiptext-after"/></a> 


<a href="../element/index.html#Package.Content.Application.Liability.PercentOwned.Owner" class="tooltip-parent">Package.Content.Application.Liability.PercentOwned.Owner<span class="tooltiptext-under">The details of the ownership of this liability, either as borrower or guarantor.</span> <span class="tooltiptext-after"/></a> 


<a href="../attribute/index.html#Package.Content.Application.Liability.PercentOwned.Owner.OwnerType" class="tooltip-parent">Package.Content.Application.Liability.PercentOwned.Owner.OwnerType<span class="tooltiptext-under">Describes whether the linked party is a borrower or guarantor on this liability.</span> <span class="tooltiptext-after"/></a> 


<a href="../attribute/index.html#Package.Content.Application.Liability.PercentOwned.Owner.Percent" class="tooltip-parent">Package.Content.Application.Liability.PercentOwned.Owner.Percent<span class="tooltiptext-under">Captures the percentage of the liability that the linked party is responsible for.</span> <span class="tooltiptext-after"/></a> 


<a href="../attribute/index.html#Package.Content.Application.Liability.PercentOwned.Owner.x_Party" class="tooltip-parent">Package.Content.Application.Liability.PercentOwned.Owner.x_Party<span class="tooltiptext-under">A reference to a party that is an responsible for the liability.</span> <span class="tooltiptext-after"/></a> 


<a href="../attribute/index.html#Package.Content.Application.Liability.PercentOwned.Proportions" class="tooltip-parent">Package.Content.Application.Liability.PercentOwned.Proportions<span class="tooltiptext-under">Flag to indicate whether the ownership proportions are equal across all owners, specified individually for each one, or not specified.</span> <span class="tooltiptext-after"/></a> 


<a href="../attribute/index.html#Package.Content.Application.LoanDetails.FeaturesSelected.OffsetAccount.x_Account" class="tooltip-parent">Package.Content.Application.LoanDetails.FeaturesSelected.OffsetAccount.x_Account<span class="tooltiptext-under">A cross reference to an account (a Non Real Estate Asset element) to be used as the offset account.</span> <span class="tooltiptext-after"/></a> 


