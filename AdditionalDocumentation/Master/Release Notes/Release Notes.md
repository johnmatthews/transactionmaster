Release notes are provided for each Transaction Schema Derived from this Master.

<br>

- [LIXI Credit Application for Australia (CAL 2.6.15) Release Notes](../../cal/2_6_15/Release_Notes/index.html)
- [LIXI Document Preparation and Settlements (DAS 2.2.15) Release Notes](../../das/2_2_15/Release_Notes/index.html)
- [LIXI Credit Application for New Zealand (CNZ 2.0.15) Release Notes](../../cnz/2_0_15/Release_Notes/index.html)
- [LIXI Credit Decision (CDA 2.0.13) Release Notes](../../cda/2_0_13/Release_Notes/index.html)
- [LIXI Serviceability (SVC 0.0.4) Release Notes](../../svc/2_0_4/Release_Notes/index.html)
