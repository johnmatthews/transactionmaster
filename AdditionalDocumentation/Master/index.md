<sup>This documentation was created the_current_time</sup>

![Logo](./img/lixi_logo_100.png)

### LIXI Master-this_version_number_dots Overview

This documentation describes the LIXI Master Schema (version this_version_number_dots). The announcement page for this release can be found on the LIXI web site: [LIXI Master Schema (version this_version_number_dots)](https://lixi.org.au/lixi-standards/lixi-master-schema-this_version_number_underscores/). This LIXI Master Schema is the source from which all Transaction Schemas are derived, and as such, no message instances should be created from this Master Schema, instead a derived Transaction Schema should be used for this purpose. The following Transaction Schemas have been derived from this Master Schema:
<br>
<br>

- [LIXI Credit Application for Australia (CAL cal_version_number_dots) Standard](../../cal/cal_version_number_underscores/index.html)
- [LIXI Document Preparation and Settlements (DAS das_version_number_dots) Standard](../../das/das_version_number_underscores/index.html)
- [LIXI Credit Application for New Zealand (CNZ cnz_version_number_dots) Standard](../../cnz/cnz_version_number_underscores/index.html)
- [LIXI Credit Decision (CDA cda_version_number_dots) Standard](../../cda/cda_version_number_underscores/index.html)
- [LIXI Serviceability (SVC svc_version_number_dots) Standard](../../svc/svc_version_number_underscores/index.html)

### Purpose & Intended Audience

This documentation provides the data dictionary, glossary and detailed specifications that describe the LIXI Master Schema. The intended audience members are those Licensees or Members that are planning to implement, or have already implemented a version of a LIXI Transaction Schema derived from a LIXI Master Schema (CAL, DAS, CNZ, CDA, or SVC 2.x).

### Release Contents

All the components of this release are available to Licensees and Members in the secure area of the LIXI Website [here](https://lixi.org.au/lixi-standards/lixi-master-schema-this_version_number_underscores/).

### Root Element

The Root Element of a LIXI conformant message is the '[Package](../this_version_number_underscores/element/index.html#Package)' Element.

### Change Management

The source code for the LIXI Standards are now hosted in [LIXILab](https://standards.lixi.org.au/) (LIXI's GitLab Repository). Please contact LIXI if you would like access. To get further information on the Change Management Processes for this standard, or to request a change, please visit the Change Management page on the LIXI website [here](http://lixi.org.au/dc-cal-2-0-change-request/).

### Statement of Proprietary Information

This standard is confidential to LIXI Limited and may not be disclosed, duplicated, or used for any purpose, in whole or in part, without the prior consent of LIXI Limited. Both the XML Schema Definition files and associated documentation are licensed under the LIXI End User Licence Agreement (EULA) that can be found here: [LIXI-End-User-Licence-Agreement](https://lixi.org.au/assets/LIXI-End-User-Licence-Agreement-2017-11.pdf). An introduction to this EULA is available here: [Introduction to EULA 2013 10 V.2.pdf](https://cdn.lixi.org.au/assets/Introduction-to-EULA-2013-10-V.2.pdf).	