Deprecated item logs are provided for each Transaction Schema Derived from this Master.

<br>

- [LIXI Credit Application for Australia (CAL 2.6.15) Deprecated Item Log](../../cal/2_6_15/Deprecated/index.html)
- [LIXI Document Preparation and Settlements (DAS 2.2.15) Deprecated Item Log](../../das/2_2_15/Deprecated/index.html)
- [LIXI Credit Application for New Zealand (CNZ 2.0.15) Deprecated Item Log](../../cnz/2_0_15/Deprecated/index.html)
- [LIXI Credit Decision (CDA 2.0.13) Deprecated Item Log](../../cda/2_0_13/Deprecated/index.html)
- [LIXI Serviceability (SVC 0.0.4) Deprecated Item Log](../../svc/2_0_4/Deprecated/index.html)