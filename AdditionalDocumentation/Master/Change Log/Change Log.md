Change logs are provided for each Transaction Schema Derived from this Master.

<br>

- [LIXI Credit Application for Australia (CAL 2.6.15) Change Log](../../cal/2_6_15/Change_Log/index.html)
- [LIXI Document Preparation and Settlements (DAS 2.2.15) Change Log](../../das/2_2_15/Change_Log/index.html)
- [LIXI Credit Application for New Zealand (CNZ 2.0.15) Change Log](../../cnz/2_0_15/Change_Log/index.html)
- [LIXI Credit Decision (CDA 2.0.13) Change Log](../../cda/2_0_13/Change_Log/index.html)
- [LIXI Serviceability (SVC 0.0.4) Change Log](../../svc/2_0_4/Change_Log/index.html)