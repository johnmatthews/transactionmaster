import requests, json, sys, os, datetime, threading
from lxml import etree
import time
start = time.time()

print ("Running Tests...\n")

# URL of the API
url = "https://f91huja9s2.execute-api.ap-southeast-2.amazonaws.com/dev/main"

# request headers, including the API key
headers = {'x-api-key': sys.argv[1]}

# load schema
read_path =  os.path.join(os.path.dirname(__file__), 'LIXI-Master-Schema.xsd')

with open(read_path, 'r', encoding='UTF-8') as myfile:
    my_data = myfile.read()

# test names
my_array = [
    'attribute_order',
    'attribute_paths',
    'complextype_use',
    'complextypes_exist',
    'element_order',
    'element_paths',
    'enumeration_order',
    'enumeration_paths',
    'simpletype_order',
    'simpletype_use',
    'simpletypes_exist',
    'validate_schema']



def my_function (test):
    payload = json.dumps({'test': test, 'data':my_data})
    response = requests.post(url, headers=headers, data=payload).text
    print(test)
    print(response)
    
from multiprocessing.dummy import Pool as ThreadPool 
pool = ThreadPool(12) 
results = pool.map(my_function, my_array)

end = time.time()
print("Tests Complete.")
print("   - Total Time:                      " + str(end - start))  
