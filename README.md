## Synopsis

This project is used to manage the LIXI Master Schema from which all LIXI2 
Transaction Schema are derived. 
It revolves around the one file: 'LIXI-Master-Schema.xsd'.

## Contributors

Initially, the LIXI Master schema will be only modified by LIXI employees. If 
LIXI members need to have changes made, they can raise a GIT Issue.

## License 

The information contained in this project is confidential to LIXI Limited and 
may not be disclosed, duplicated, or used, for any purpose, in whole or in part, 
without the prior consent of LIXI Limited. 
The XML Schema Definition files and associated documentation including issues
and wiki pages are licensed under the LIXI End User Licence Agreement (EULA) 
that can be found on the LIXI website: 
(https://lixi.org.au/join-lixi/lixi-licensing-details/).