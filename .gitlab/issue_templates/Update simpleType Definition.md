# Problem / Requirement Statement

Update the simpleType definition of 'xxx' to:

''

# Solution
## Overview

Update Definitions of simpleType:

- xxx


# Related Links

- [Latest documentation for simpleType xxx in Master](https://smedia.lixi.org.au/standards-docs.html?standard=master&version=current&component=simpletype&item=xxx)