# Problem / Requirement Statement

State the problem or business requirement in business terminology here. Avoid 'solution-speak' (such as 'add new element' or 'add new attribute') in this section, as this is related to the solution rather than the problem or requirement.

# Solution
## Overview

This will be completed once the requirement gathering process is complete enough for an implementation to be suggested.

## Detail

This will be completed once the requirement gathering process is complete enough for an implementation to be suggested.

## Definitions

|Name|Definition|
|---|---|
|||

## Data Dictionary

|Item Type|Name/Value|minOccurs|maxOccurs|type|use|documentation|path|label|transaction|
|---|---|---|---|---|---|---|---|---|---|---|
||||||||||||

## Master Schema Snippet

```xml
```

# Related Links

- [Latest documentation for attribute xxx in Master](https://smedia.lixi.org.au/standards-docs.html?standard=master&version=current&component=attribute&item=xxx)
- [Latest documentation for element xxx in Master](https://smedia.lixi.org.au/standards-docs.html?standard=master&version=current&component=element&item=xxx)
- [Latest documentation for simpleType xxx in Master](https://smedia.lixi.org.au/standards-docs.html?standard=master&version=current&component=simpletype&item=xxx)