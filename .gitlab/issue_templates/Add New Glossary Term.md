# Problem / Requirement Statement

Glossary term 'xxx' (abbreviation yyy) is not defined.


# Solution
## Overview

Add New Term to Glossary:

'xxx'

Definition:

'Add definition here'

# Related Links

- [Latest documentation for glossary term xxx in Master](https://smedia.lixi.org.au/standards-docs.html?standard=master&version=current&component=glossary&item=xxx)